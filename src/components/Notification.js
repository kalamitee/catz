import React, { Component } from 'react';
import { Alert } from 'react-bootstrap';

class Notification extends Component {
    render() {
        if (!this.props.show) {
            return null;
        }

        return (
            <Alert variant="danger">
                T_T. Apologies but we could not load new cats for you at this time! Miau!
            </Alert>
        );
    }
}

export default Notification;
