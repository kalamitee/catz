import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card } from 'react-bootstrap';

class Cat extends Component {
    renderHeader() {
        if (!this.props.headerContent) {
            return null;
        }
        return (<Card.Header>{this.props.headerContent}</Card.Header>);
    }

    renderBody() {
        if (!this.props.showDetails) {
            return (
                <Card.Body>
                    <Button as={Link} to={`/${this.props.cat.id}`} block>
                        View details
                    </Button>
                </Card.Body>
            );
        }

        const breedDetails = this.props.cat.breeds[0];
        return (
            <Card.Body>
                <Card.Title as="h4">{breedDetails.name}</Card.Title>
                <Card.Title as="h5">Origin: {breedDetails.origin}</Card.Title>
                <Card.Title as="h6">{breedDetails.temperament}</Card.Title>
                <Card.Text>{breedDetails.description}</Card.Text>
            </Card.Body>
        );
    }

    render() {
        return (
            <Card style={{marginBottom: '1rem'}}>
                {this.renderHeader()}
                <Card.Img variant="top" src={this.props.cat.url}></Card.Img>
                {this.renderBody()}
            </Card>
        );
    }
}

export default Cat;
