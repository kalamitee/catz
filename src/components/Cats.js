import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';
import Cat from './Cat'

class Cats extends Component {
    renderCats() {
        if (!this.props.cats.length) {
            return <Col md={3} sm={6}>No cats available</Col>
        }

        return this.props.cats.map((cat) => {
            return <Col md={3} sm={6} key={cat.id}>
                <Cat cat={cat} showDetails={false}></Cat>
            </Col>;
        });
    }

    render() {
        return (
            <Row style={{marginBottom: '2rem'}}>{this.renderCats()}</Row>
        );
    }
}

export default Cats;
