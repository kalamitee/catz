import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Container, Button } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';
import { getCatById } from '../data/data';
import Cat from '../components/Cat';
import Notification from '../components/Notification';

class CatDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cat: {},
            isLoading: true,
        };
    }

    componentDidMount() {
        this.initialize();
    }

    async initialize() {
        this.setState({hasError: false});
        try {
            this.setState({cat: await getCatById(this.props.match.params.id)});
        } catch (error) {
            this.setState({hasError: true});
        } finally {
            this.setState({isLoading: false});
        }
    }

    renderDetails() {
        if (!this.state.cat || this.state.hasError) {
            return <Notification show={this.state.hasError} />
        }

        return (
            <Cat 
                cat={this.state.cat} 
                showDetails={true}
                headerContent={<Button as={Link} to={`/?breed=${this.state.cat.breeds[0].id}`}>Back</Button>}
            ></Cat>
        );
    }

    render() {
        return (
            <Container>
                {this.state.isLoading ? <h3>Loading...</h3> : this.renderDetails()}
            </Container>
        );
    }
}

export default withRouter(CatDetails);
