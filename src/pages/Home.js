import React, { Component } from 'react';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';
import { getBreeds, getCats } from '../data/data';
import Cats from '../components/Cats';
import Notification from '../components/Notification';

class Home extends Component {
    catsFilter = {page: 1, limit: 10};
    selectedBreed = '';

    constructor(props) {
        super(props);
        this.state = {
            breeds: [],
            cats: [],
            isLoading: true,
            allCatsLoaded: false,
            hasError: false
        };
    }

    componentDidMount() {
        this.initialize();
    }

    async initialize() {
        try {
            this.setState({breeds: await getBreeds(), hasError: false});
            this.selectedBreed = new URLSearchParams(this.props.location.search).get('breed') || '';

            if (this.selectedBreed) {
                this.catsFilter.breed_id = this.selectedBreed;
                this.loadCats();
            } else {
                this.setState({isLoading: false});    
            }
        } catch (error) {
            this.setState({hasError: true, isLoading: false});
        }
    }

    async loadCats() {
        this.setState({isLoading: true, hasError: false});
        try {
            const cats = await getCats(this.catsFilter);
            const existingCats = this.state.cats.slice();
            const newCats = cats.filter((cat) => existingCats.every((existingCat) => existingCat.id !== cat.id));
            this.setState({cats: existingCats.concat(newCats), allCatsLoaded: !newCats.length});
        } catch (error) {
            this.setState({hasError: true});
        } finally {
            this.setState({isLoading: false});
        }
    }

    loadMore() {
        ++this.catsFilter.page;
        this.loadCats();
    }

    handleChange(event) {
        this.setState({cats: [], allCatsLoaded: false});
        this.selectedBreed = event.target.value;
        if (!this.selectedBreed) {
            return;
        }

        this.catsFilter.breed_id = this.selectedBreed;
        this.loadCats();
    }

    renderBreeds() {
        const breeds = this.state.breeds.slice();
        return breeds.map((breed) => <option value={breed.id} key={breed.id}>{breed.name}</option>);
    }

    renderButton() {
        if (this.state.allCatsLoaded) {
            return null;
        }

        return (
            <Row>
                <Col md={3} sm={6}>
                    <Button
                        variant="success"
                        onClick={() => this.loadMore()}
                        disabled={this.state.isLoading || !this.state.cats.length}
                    >
                        {this.state.isLoading && (this.state.cats.length || this.selectedBreed) ? 'Loading cats...' : 'Load more'}
                    </Button>
                </Col>
            </Row>
        );
    }

    render() {
        return (
            <Container>
                <Notification show={this.state.hasError} />
                <h1>Cat Browser</h1>
                <Row>
                    <Col md={3} sm={6}>
                        <Form.Group>
                            <Form.Label>Breed</Form.Label>
                            <Form.Control 
                                as="select"
                                value={this.selectedBreed}
                                onChange={(event) => this.handleChange(event)}
                                disabled={this.state.isLoading}
                            >
                                <option value="">Select breed</option>
                                {this.renderBreeds()}
                            </Form.Control>
                        </Form.Group>
                    </Col>
                </Row>
                
                <Cats cats={this.state.cats}></Cats>

                {this.renderButton()}
            </Container>
        );
    }
}

export default withRouter(Home);
