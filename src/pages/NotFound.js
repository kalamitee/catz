import React, { Component } from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

class NotFound extends Component {
    render() {
        return(
            <Container>
                <Row>
                    <Col>
                        <h2>Cat.. errr... I mean, page not found.</h2>
                        <Button as={Link} to="/">Go back to home page</Button>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default NotFound;
