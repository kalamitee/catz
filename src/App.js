import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import CatDetails from './pages/CatDetails';
import Home from './pages/Home';
import NotFound from './pages/NotFound';

function App() {
  return (
    <Router>
      <div style={{margin: '2rem 0'}}>
        <Switch>
          <Route path="/:id">
            <CatDetails />
          </Route>

          <Route path="/">
            <Home />
          </Route>

          <Route path="*">
            <NotFound />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
