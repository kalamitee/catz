import axios from 'axios';

const BASE_URL = 'https://api.thecatapi.com/v1';

export const getBreeds = () => {
    return axios.get(`${BASE_URL}/breeds`)
        .then((response) => response.data);
}

export const getCats = (filter) => {
    return axios.get(`${BASE_URL}/images/search`, {params: filter})
        .then((response) => response.data);
}

export const getCatById = (id) => {
    return axios.get(`${BASE_URL}/images/${id}`)
        .then((response) => response.data);
}
